<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('/');
Route::get('/about', 'AboutController@about')->name('about');
Route::get('/katalog', 'KatalogController@koleksi')->name('katalog');
Route::get('/lokasi', 'KatalogController@lokasi')->name('lokasi');
Route::get('/informasi', 'KatalogController@informasi')->name('informasi');


// register dan login//
Route::get('/register', 'PostController@register')->name('register');
Route::post('/data/register', 'PostController@store');
Route::get('/login', 'PostController@login')->name('login');
Route::post('/simpan', 'PostController@simpan')->name('simpan');
Route::get('/logout', 'PostController@logout')->name('logout');
Route::get('/dashboard', 'PostController@index')->name('dashboard');

Route::group(['middleware' => ['auth']], function () {
        //
        
    // Manage Akun//
    Route::name('manage-akun.')->prefix('manage-akun')->group(function() {
        Route::get('', 'AnggotaController@index')->name('index');
        Route::get('create', 'AnggotaController@create')->name('create');
        Route::post('store', 'AnggotaController@store')->name('store');
        Route::get('show/{users_id}', 'AnggotaController@show')->name('show');
        Route::get('edit/{users_id}', 'AnggotaController@edit')->name('edit');
        Route::put('update/{id}', 'AnggotaController@update')->name('update');
        Route::get('status/{id}','AnggotaController@status')->name('status');
    });

    // Role Anggota//
    Route::name('roles.')->prefix('roles')->group(function() {
        Route::get('', 'RolesController@index')->name('index');
        Route::get('add', 'RolesController@add')->name('add');
        Route::post('store', 'RolesController@store')->name('store');
        Route::get('edit/{id}', 'RolesController@edit')->name('edit');
        Route::put('update/{id}', 'RolesController@update')->name('update');
        Route::delete('destroy/{id}', 'RolesController@destroy')->name('destroy');
    });


    // Manage Buku //
    Route::name('master-buku.')->prefix('master-buku')->group(function() {
        Route::get('', 'BukuController@index')->name('index');
        Route::get('blank', 'BukuController@blank')->name('blank');
        Route::get('nonaktif', 'BukuController@nonaktif')->name('nonaktif');
        Route::get('create', 'BukuController@create')->name('create');
        Route::post('load', 'BukuController@load')->name('load');
        Route::get('show/{buku_id}', 'BukuController@show')->name('show');
        Route::get('edit/{buku_id}', 'BukuController@edit')->name('edit');
        Route::put('update/{buku_id}', 'BukuController@update')->name('update');
        Route::delete('destroy/{buku_id}', 'BukuController@destroy')->name('destroy');
        Route::get('status/{id}','BukuController@status')->name('status');
        Route::get('ambil/{id}','BukuController@ambil')->name('ambil');
    });

    // Peminjaman Buku //
    Route::name('pinjam-buku.')->prefix('pinjam-buku')->group(function() {
        Route::get('', 'PeminjamanController@index')->name('index');
        Route::get('cek/{id}', 'PeminjamanController@cek')->name('cek');
        Route::get('form', 'PeminjamanController@form')->name('form');
        Route::post('save', 'PeminjamanController@save')->name('save');
        Route::get('setujui/{id}', 'PeminjamanController@setujui')->name('setujui');
        Route::get('tolak/{id}', 'PeminjamanController@tolak')->name('tolak');
    });

    // Pengembalian Buku //
    Route::name('pengembalian-buku.')->prefix('pengembalian-buku')->group(function() {
        Route::get('', 'PengembalianController@index')->name('index');
        Route::get('/{id}', 'PengembalianController@kembalikan')->name('kembalikan');
        Route::get('edit/{pinjam_id}', 'PengembalianController@edit')->name('edit');
        Route::put('update', 'PengembalianController@update')->name('update');
    });

    // Laporan Peminjaman Buku //
    Route::name('laporan.')->prefix('laporan')->group(function() {
        Route::get('', 'LaporanController@index')->name('index');
    });

});


