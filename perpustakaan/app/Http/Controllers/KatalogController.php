<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KatalogController extends Controller
{
    public function koleksi()
    {
        return view('halaman.koleksi');
    }

    public function lokasi()
    {
        return view('halaman.lokasi');
    }

    public function informasi()
    {
        return view('halaman.informasi');
    }
}
