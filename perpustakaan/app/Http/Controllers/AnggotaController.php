<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class AnggotaController extends Controller
{
    //
    
    public function index()
    {
        $userdata = DB::table('users')
        ->join('roles as r', 'r.id','=','users.roles_id')
        ->select('users.*', 'r.peran')
        ->get(['users.*', 'r.id as roles_id']);

        return view('anggota.index',compact('userdata'));
    }

    public function create()
    {
        $ambilroles = DB::table('roles')->get();
        return view('anggota.create', compact('ambilroles'));
    }

    public function store(Request $request)
    {
        $request->validate(
            [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'status_akun' => 'required',
            'profil' => 'required|mimes:png,jpg,jpeg',
            'nik' => 'required', 
            'roles_id' => '',
            ],
            [
                'name.required' => 'name harus diisi',
                'email.required'  => 'email harus diisi',
                'password.required'  => 'password harus diisi',
                'status_akun.required'  => 'peran harus diisi',
                'profil.required' => 'profil harus diisi',
                'nik.required'  => 'nik harus diisi', 
            ],
            
        );

        $fileName = time().'.'.$request->profil->extension();

        DB::table('users')->insert(
            [
                'name' => $request['name'], 
                'email' => $request['email'],
                'password' => bcrypt($request['password']),
                'status_akun' => $request['status_akun'],
                'profil' => $fileName,
                'nik' => $request['nik'],
                'roles_id' => $request['roles_id'],

            ]
        );

        $request->profil->move(public_path('image'), $fileName);
  
        return redirect()->route('manage-akun.index')->with('best', 'Data  Anggota berhasil ditambahkan!');;
    }

    public function show($id)
    {
        $userdata = DB::table('users')->where('id', $id)->get();
        $id = $userdata->id;

        return view('anggota.show', compact('userdata', 'id'));
    }

    public function edit($id)
    {
        $userdata = DB::table('users')->where('id', $id)->get();

        return view('anggota.edit', compact('userdata'));
    }

    public function update(Request $request, $id)
    {
        $request->validate(
            [
            'name' => 'required',
            'status_akun' => 'required',
            'nik' => 'required', 
            'roles_id' => 'required',
            ],
            [
                'name.required' => 'name harus diisi',
                'status_akun.required'  => 'status harus diisi',
                'nik.required'  => 'nik harus diisi', 
                'roles_id.required' => 'roles harus diisi',
            ]
        );

        DB::table('users')
        ->where('id',$id)
        ->update(
            [
                'name' => $request['name'], 
                'status_akun' => $request['status_akun'],
                'nik' => $request['nik'],
                'roles_id' => $request['roles_id'],
              ]
            );
        return redirect()->route('manage-akun.index')->with('success', 'Data Berhasil Disimpan');;
    }

    public function status($id)
    {
        $userdata = DB::table('users')->where('id',$id)->first(); 
        $datacek = $userdata->status_akun;
        if ($datacek == 1){
            DB::table('users')->where('id',$id)->update([
                'status_akun'=>0
            ]);
        } else {
            DB::table('users')->where('id',$id)->update([
                'status_akun'=>1
            ]);
        }
        return redirect()->route('manage-akun.index')->with('success', 'Status Anggota Berhasil diubah');
    }


}
