<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RolesController extends Controller
{
    public function index()
    {
        
        $rolesdata = DB::table('roles')->get();
        return view('roles.index',compact('rolesdata'));
    }

    public function add()
    {
        return view('roles.add');
    }

    public function store(Request $request)
    {
        $request->validate(
            [
            'peran' => 'required',
            ],
            [
                'peran.required' => 'peran harus diisi',
            ],
            
        );

        DB::table('roles')->insert(
            [
                'peran' => $request['peran'], 
            ]
        );
  
        return redirect('/roles')->with('best', 'Data  Roles berhasil ditambahkan!');;
    }

    public function edit($id)
    {
        $rolesdata = DB::table('roles')->where('id', $id)->first();

        return view('roles.edit', compact('rolesdata'));
    }

    public function update(Request $request, $id)
    {
        $request->validate(
            [
            'peran' => 'required',
            ],
            [
                'peran.required' => 'peran harus diisi',
            ]
        );

        DB::table('roles')
              ->where('id', $id)
              ->update(
                  [
                    'peran' => $request['peran'], 
                  ]
                );

        return redirect('/roles')->with('best', 'Data Berhasil Disimpan');
    }

    public function destroy($id)
    {
        DB::table('roles')->where('id', '=', $id)->delete();

        return redirect('/roles')->with('best', 'Data Roles Berhasil Dihapus');
    }
}
