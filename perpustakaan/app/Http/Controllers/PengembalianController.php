<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PengembalianController extends Controller
{
    
    public function index()
    {
        if (Auth::user()->roles_id ==1) {
            $pinjamdata = DB::table('pinjam')
            ->join('buku as b', 'b.id', '=','pinjam.buku_id')
            ->select('pinjam.*', 'b.judul')
            ->whereIn('status',[1,3])
            ->get(['pinjam.*', 'b.id as buku_id']);
        } else {
            $pinjamdata = DB::table('pinjam')
            ->join('buku as b', 'b.id', '=','pinjam.buku_id')
            ->select('pinjam.*', 'b.judul')
            ->where('user_id', Auth::user()->name)
            ->whereIn('status',[1,3])
            ->get(['pinjam.*', 'b.id as buku_id']);
        }

        return view('pengembalian.index',compact('pinjamdata'));
    }

    public function kembalikan($id)
    {
        $pinjamdata = DB::table('pinjam')->where('id', $id)->update(['status'=>3]);
        return redirect()->route('pengembalian-buku.index');
    }

    public function edit($id)
    {
        $pinjamdata = DB::table('pinjam')
        ->join('buku as b', 'b.id', '=','pinjam.buku_id')
        ->select('pinjam.*', 'b.judul')
        ->whereIn('status',[1,3])
        ->get(['pinjam.*', 'b.id as buku_id']);

        return view('pengembalian.edit',compact('pinjamdata'));

    }

    public function update(Request $request)
    {
        $request->validate(
            [
            'tanggal_pengembalian' => 'required',
            ],
            [
                'tanggal_pengembalian.required' => 'tanggal harus diisi',
            ]
        );

        DB::table('pinjam')
              ->update(
                  [
                    'tanggal_pengembalian' => date('Y-m-d H:i:s'), 
                  ]
                );

        return redirect('/pengembalian-buku')->with('best', 'Data Berhasil Disimpan');
    }

  
}
