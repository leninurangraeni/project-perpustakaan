<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LaporanController extends Controller
{
    public function index()
    {
            $datalaporan = DB::table('pinjam')
            ->join('buku as b', 'b.id', '=','pinjam.buku_id')
            ->join('m_status as m', 'm.kode', '=','pinjam.status')
            ->select('pinjam.*', 'b.judul', 'm.kode','m.nama')
            ->get(['pinjam.*', 'b.id as buku_id', 'm.id as status']);

        return view('laporan.index',compact('datalaporan'));
    }
}
