<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Pinjam;



class PeminjamanController extends Controller
{

    public function index()
    {
        if (Auth::user()->roles_id ==1) {
            $pinjamdata = DB::table('pinjam')
            ->join('buku as b', 'b.id', '=','pinjam.buku_id')
            ->select('pinjam.*', 'b.judul')
            ->get(['pinjam.*', 'b.id as buku_id']);
        } else {
            $pinjamdata = DB::table('pinjam')
            ->join('buku as b', 'b.id', '=','pinjam.buku_id')
            ->select('pinjam.*', 'b.judul')
            ->where('user_id', Auth::user()->name)
            ->get(['pinjam.*', 'b.id as buku_id']);
        }

        return view('pinjam.index',compact('pinjamdata'));
    }

    public function cek($id)
    {
        $cek = DB::table('buku')->where('id', $id)->where('stok_buku','>',0)->where('status_buku',1)->count();
        if ($cek > 0) {
            return redirect()->route('pinjam-buku.form'); 

        } else {
            return redirect()->route('master-buku.index');
        }
        
    }

    public function form()
    {
        $ambilbuku =  DB::table('buku')->get();
        return view('pinjam.form', compact('ambilbuku'));
    }

    // public function tambah($id)
    // {
    //     DB::table('test')->insert(
    //         [
    //         'user_id'=>Auth::user()->id,
    //         'buku_id'=>$id,
    //         'created_at' =>date('Y-m-d H:i:s'), 
    //         ]);
    //     return redirect()->route('pinjam-buku.index');
    // }

    public function save(Request $request)
    {
        $request->validate(
            [
            'user_id' => '',
            'buku_id' => '',
            'jenis_transaksi' => 'required',
            'tanggal_pengajuan' => 'required',
            'tanggal_pengembalian' => '',
            ],
            [
 
                'jenis_transaksi.required' => 'data harus diisi',
                'tanggal_pengajuan.required'  => 'data harus diisi',
            ],
        );

        DB::table('pinjam')->insert(
            [
            'user_id'=>Auth::user()->name,
            'buku_id'=>$request['buku_id'],
            'jenis_transaksi' => $request['jenis_transaksi'], 
            'tanggal_pengajuan' => date('Y-m-d H:i:s'), 
            'tanggal_pengembalian' => $request['tanggal_pengembalian'], 
            'status' => $request['status'], 
            ]);
        return redirect()->route('pinjam-buku.index');
    }

    public function setujui($id)
    {
        $pinjamdata= DB::table('pinjam')->where('id', $id)->update(['status'=>1]);
        return redirect()->route('pinjam-buku.index');
    }

    public function tolak($id)
    {
        $pinjamdata= DB::table('pinjam')->where('id', $id)->update(['status'=>2]);
        return redirect()->route('pinjam-buku.index');
    }
}
