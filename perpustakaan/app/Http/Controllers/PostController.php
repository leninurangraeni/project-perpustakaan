<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    //
    public function login()
    {
            return view('auth.login');
    }

    public function simpan(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect()->route('dashboard');
        }
    }

    public function register()
    {
        return view('auth.register');
    }

    public function index()
    {
        return view('dasbor.index');
    }

    public function store(Request $request)
    {
        $request->validate(
            [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'status_akun' => 'required',
            'profil' => 'required',
            'nik' => 'required',
            'roles_id' => '',
            ],
            [
                'name.required' => 'name harus diisi',
                'email.required'  => 'email harus diisi',
                'password.required'  => 'password harus diisi',
                'status_akun.required'  => 'peran harus diisi',
                'profil.required'  => 'profil harus diisi',
                'nik.required'  => 'nik harus diisi',
            ],

        );

        $fileName = time().'.'.$request->profil->extension();

        DB::table('users')->insert(
            [
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => bcrypt($request['password']),
                'status_akun' => $request['status_akun'],
                'profil' => $fileName,
                'nik' => $request['nik'],
                'roles_id' => $request['roles_id'],
            ]
        );

        $request->profil->move(public_path('image'), $fileName);

        return redirect()->route('register')->with('best', 'Register berhasil dilakukan');
    }

    public function logout(Request $request)
    {
        Auth::logout();
        request()->session()->invalidate();
        request()->session()->regenerateToken();
        return redirect('/login');
    }



}
