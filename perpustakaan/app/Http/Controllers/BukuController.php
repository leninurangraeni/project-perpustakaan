<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BukuController extends Controller
{
    //
    public function index()
    {
        $databuku = DB::table('buku')->get();
        return view('buku.index', compact('databuku'));
    }

    public function blank()
    {
        $databuku = DB::table('buku')->where('stok_buku', '<', 1)->get();
        return view('buku.index', compact('databuku'));
    }
   
    public function nonaktif()
    {
        $databuku = DB::table('buku')->where('status_buku', '!=', 1)->get();
        return view('buku.index', compact('databuku'));
    }

    public function create()
    {
        return view('buku.create');
    }

    public function load(Request $request)
    {
        $request->validate(
            [
            'judul' => 'required',
            'deskripsi' => 'required',
            'pengarang' => 'required',
            'tahun' => 'required',
            'kode_buku' => 'required', 
            'stok_buku' => 'required', 
            'lokasi' => 'required',
            'status_buku' => '',
            ],
            [
                'judul.required' => 'judul harus diisi',
                'deskripsi.required'  => 'deskripsi harus diisi',
                'pengarang.required'  => 'pengarang harus diisi',
                'tahun.required'  => 'tahun harus diisi',
                'kode_buku.required'  => 'kode_buku harus diisi', 
                'stok_buku.required'  => 'stok_buku harus diisi',
                'lokasi.required'  => 'lokasi harus diisi', 
            ],
        );

        DB::table('buku')->insert(
            [
                'judul' => $request['judul'], 
                'deskripsi' => $request['deskripsi'],
                'pengarang' => $request['pengarang'],
                'tahun' => $request['tahun'],
                'kode_buku' => $request['kode_buku'],
                'stok_buku' => $request['stok_buku'],
                'lokasi' => $request['lokasi'],
                'status_buku' => $request['status_buku'],
            ],
        );
        return redirect()->route('master-buku.index')->with('success', 'Data Berhasil Disimpan');
    }

    public function show($id)
    {
        $databuku = DB::table('buku')->where('id', $id)->first();

        return view('buku.show', compact('databuku'));
    }

    public function edit($id)
    {
        $databuku = DB::table('buku')->where('id', $id)->first();

        return view('buku.edit', compact('databuku'));
    }

    public function update(Request $request, $id)
    {
        $request->validate(
            [
             'judul' => 'required',
             'deskripsi' => 'required',
             'pengarang' => 'required',
             'tahun' => 'required',
             'kode_buku' => 'required', 
             'stok_buku' => 'required',
             'lokasi' => 'required',
             'status_buku' => '',
            ],
            [
                'judul.required' => 'judul harus diisi',
                'deskripsi.required'  => 'deskripsi harus diisi',
                'pengarang.required'  => 'pengarang harus diisi',
                'tahun.required'  => 'tahun harus diisi',
                'kode_buku.required'  => 'kode_buku harus diisi', 
                'stok_buku.required'  => 'stok_buku harus diisi',
                'lokasi.required'  => 'lokasi harus diisi', 
            ]
        );

        DB::table('buku')
              ->where('id', $id)
              ->update(
                  [
                    'judul' => $request['judul'], 
                    'deskripsi' => $request['deskripsi'],
                    'pengarang' => $request['pengarang'],
                    'tahun' => $request['tahun'],
                    'kode_buku' => $request['kode_buku'],
                    'stok_buku' => $request['stok_buku'],
                    'lokasi' => $request['lokasi'],
                    'status_buku' => $request['status_buku'],
                  ]
                );
                
        return redirect()->route('master-buku.index')->with('success', 'Data Berhasil Disimpan');
    }

    public function destroy($id)
    {
        DB::table('buku')->where('id', '=', $id)->delete();

        return redirect()->route('master-buku.index')->with('success', 'Data Buku Berhasil Dihapus');
    }

    public function status($id)
    {
        $databuku = DB::table('buku')->where('id', $id)->first(); 
        $cekdata = $databuku->status_buku;
        if ($cekdata == 1){
            DB::table('buku')->where('id', $id)->update([
                'status_buku'=>0
            ]);
        } else {
            DB::table('buku')->where('id', $id)->update([
                'status_buku'=>1
            ]);
        }
        return redirect()->route('master-buku.index')->with('success', 'Status Buku Berhasil diubah');
    }

    public function ambil($id)
    {
        $buku = DB::table('buku')->where('id', $id)->first();
        $stok_now = $buku->stok_buku;
        $stok_new = $stok_now - 1;
        
        DB::table('buku')->where('id', $id)->update([
            'stok_buku'=>$stok_new,
        ]);
        return redirect()->route('master-buku.index')->with('success', 'Stok Buku Berhasil diubah');
    }
}
