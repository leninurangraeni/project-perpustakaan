<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pinjam extends Model
{

    protected $table = "pinjam";
    protected $fillable = 
    [
        'user_id', 'buku_id', 'jenis_transaksi', 'tanggal_pengajuan', 'tanggal_pengembalian', 'status'
    ];

}
