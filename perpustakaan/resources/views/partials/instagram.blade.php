<div class="instagram-box">
    <div class="main-instagram owl-carousel owl-theme">
        <div class="item">
            <div class="ins-inner-box">
                <img src="{{asset('template/images/buku001.jpg')}}" alt="" />
                <div class="hov-in">
                    <a href="#"><i class="fas fa-store"></i></a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="ins-inner-box">
                <img src="{{asset('template/images/buku002.jpg')}}" alt="" />
                <div class="hov-in">
                    <a href="#"><i class="fas fa-store"></i></a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="ins-inner-box">
                <img src="{{asset('template/images/buku003.jpg')}}" alt="" />
                <div class="hov-in">
                    <a href="#"><i class="fas fa-store"></i></a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="ins-inner-box">
                <img src="{{asset('template/images/buku004.jpg')}}" alt="" />
                <div class="hov-in">
                    <a href="#"><i class="fas fa-store"></i></a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="ins-inner-box">
                <img src="{{asset('template/images/buku005.jpg')}}" alt="" />
                <div class="hov-in">
                    <a href="#"><i class="fas fa-store"></i></a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="ins-inner-box">
                <img src="{{asset('template/images/buku006.jpg')}}" alt="" />
                <div class="hov-in">
                    <a href="#"><i class="fas fa-store"></i></a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="ins-inner-box">
                <img src="{{asset('template/images/buku007.webp')}}" alt="" />
                <div class="hov-in">
                    <a href="#"><i class="fas fa-store"></i></a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="ins-inner-box">
                <img src="{{asset('template/images/buku008.jpg')}}" alt="" />
                <div class="hov-in">
                    <a href="#"><i class="fas fa-store"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
