<div id="slides-shop" class="cover-slides">
    <ul class="slides-container">
        <li class="text-center">
            <img src="{{asset('template/images/spanduk.jpg')}}" alt="">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="m-b-20"><strong>Welcome To <br> Leandy University Library</strong></h1>
                        <p class="m-b-40">See there are many books on this website <br> Get the book you want or you can borrow it</p>
                        <p><a class="btn hvr-hover" href="#">Visit Now</a></p>
                    </div>
                </div>
            </div>
        </li>
        <li class="text-center">
            <img src="{{asset('template/images/spanduk02.jpg')}}" alt="">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="m-b-20"><strong>Welcome To <br> Leandy University Library</strong></h1>
                        <p class="m-b-40">See there are many books on this website <br> Get the book you want or you can borrow it.</p>
                        <p><a class="btn hvr-hover" href="#">Visit Now</a></p>
                    </div>
                </div>
            </div>
        </li>
        <li class="text-center">
            <img src="{{asset('template/images/spanduk03.jpg')}}" alt="">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="m-b-20"><strong>Welcome To <br> Leandy University Library</strong></h1>
                        <p class="m-b-40">See there are many books on this website <br> Get the book you want or you can borrow it.</p>
                        <p><a class="btn hvr-hover" href="#">Visit Now</a></p>
                    </div>
                </div>
            </div>
        </li>
    </ul>
    <div class="slides-navigation">
        <a href="#" class="next"><i class="fas fa-angle-right" aria-hidden="true"></i></a>
        <a href="#" class="prev"><i class="fas fa-angle-left" aria-hidden="true"></i></a>
    </div>
</div>