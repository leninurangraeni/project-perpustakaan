<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
      <img src="{{asset('template/images/logo01.png')}}" alt="Library Leandy Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Leandy Library</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('image/'.Auth::user()->profil)}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{Auth::user()->name}}</a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

          <li class="nav-item">
            <a href="/dashboard" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <span class="right badge badge-danger">New</span>
              </p>
            </a>
          </li>

          @if(Auth::user()->status_akun == 1)
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Managemen Buku
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('master-buku.index') }}" class="nav-link">
                  <i class="nav-icon 	fas fa-book-open"></i>
                  <p>
                    Data Buku
                    <i class="right"></i>
                  </p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('pinjam-buku.index') }}" class="nav-link">
                  <i class="nav-icon 	fas fa-book-reader"></i>
                  <p>
                    Data Peminjaman Buku 
                    <i class="right"></i>
                  </p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('pengembalian-buku.index') }}" class="nav-link">
                  <i class="nav-icon fas fa-backspace"></i>
                  <p>
                    Data Pengembalian Buku 
                    <i class="right"></i>
                  </p>
                </a>
              </li>
            </ul>
          </li>
          @if(Auth::user()->roles_id == 1)
          <li class="nav-item">
            <a href="" class="nav-link">
              <i class="nav-icon fas fa-bullhorn"></i>
              <p>
                Mangemen Anggota
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('manage-akun.index') }}" class="nav-link">
                  <i class="nav-icon fas fa-user-alt"></i>
                  <p>Data Anggota</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('roles.index') }}" class="nav-link">
                  <i class="fas fa-th-large nav-icon"></i>
                  <p>Data Role</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{ route('laporan.index') }}" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Laporan Peminjaman 
                <span class="right"></span>
              </p>
            </a>
          </li>
          @endif
          @endif

          <li class="nav-item">
            <a href="/login" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Logout
                <i class="right "></i>
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
