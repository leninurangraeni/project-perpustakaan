<nav class="navbar navbar-expand-lg navbar-light bg-light navbar-default bootsnav">
    <div class="container">
        <!-- Start Header Navigation -->
        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-bars"></i>
        </button>
            <a class="navbar-brand" href="/"><img src="{{asset('template/images/gambaar.png')}}" class="logo" alt=""></a>
        </div>
        <!-- End Header Navigation -->
    <!-- Start Slider -->

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-menu">
            <ul class="nav navbar-nav ml-auto" data-in="fadeInDown" data-out="fadeOutUp">
                <li class="nav-item"><a class="nav-link" href="{{ route('/') }}">Home</a></li>
                <li class="nav-item active"><a class="nav-link" href="{{ route('about') }}">About Us</a></li>
                <li class="dropdown">
                    <a href="#" class="nav-link dropdown-toggle arrow" data-toggle="dropdown">Katalog</a>
                    <ul class="dropdown-menu">
                        @if (Auth::guest())
                          <li><a href="{{ route('katalog') }}">Koleksi Buku</a></li>
                          <li><a href="{{ route('lokasi') }}">Lokasi Buku</a></li>
                          <li><a href="{{ route('lokasi') }}">Informasi Peminjaman</a></li>
                        @else
                          <li><a href="{{ route('master-buku.index') }}">Detail Buku</a></li>
                          <li><a href="{{ route('lokasi') }}">Lokasi Buku</a></li>
                        @endif
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="nav-link dropdown-toggle arrow" data-toggle="dropdown">Account</a>
                    <ul class="dropdown-menu">
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            {{ Auth::user()->name }}
                            <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                            <li><a href="{{ route('logout') }}">Logout</a></li>
                        @endif
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->

        <!-- Start Atribute Navigation -->

        <!-- End Atribute Navigation -->
    </div>
    <!-- Start Side Menu -->

    <!-- End Side Menu -->
</nav>