@extends('layout.dasbor')
@section('judul')
     <h4>Selamat Datang <b>{{Auth::user()->name}}</b> <b><i class="fas fa-allergies"></i></b>
      <h4> Anda Login sebagai <b>{{Auth::user()->roles_id == 1 ? 'Admin' : 'Mahasiswa'}}</b>.</h4>
      @if(Auth::user()->status_akun == 0)
      <h4> Akun anda Belum Terverifikasi <b> 
      @endif
@endsection

@if(Auth::user()->status_akun == 1)
@section('konten')

<div class="row">
  <div class="col">
    <div class="card">
      <img class="card-img-top" src="{{asset('template/images/buku001.jpg')}}" alt="Card image cap">
      <div class="card-body">
        <h5 class="card-title">Garis Waktu</h5>
        <p class="card-text"> oleh Fiersa Besari</p>
        <a href="#" class="btn btn-primary">Add To Borrow</a>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card">
      <img class="card-img-top" src="{{asset('template/images/buku005.jpg')}}" alt="Card image cap">
      <div class="card-body">
        <h5 class="card-title">Guru Aini</h5>
        <p class="card-text">oleh Andrea Hirata</p>
        <a href="#" class="btn btn-primary">Add To Borrow</a>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card">
      <img class="card-img-top" src="{{asset('template/images/buku002.jpg')}}" alt="Card image cap">
      <div class="card-body">
        <h5 class="card-title">Jika Kita Tak Pernah Jadi Apa-Apa</h5>
        <p class="card-text">  oleh Alvin Syahrin</p>
        <a href="#" class="btn btn-primary">Add To Borrow</a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col">
    <div class="card">
      <img class="card-img-top" src="{{asset('template/images/buku008.jpg')}}" alt="Card image cap">
      <div class="card-body">
        <h5 class="card-title">Psikolgi Komunikasi</h5>
        <p class="card-text">  oleh Drs. Jalaluddudin Rakhmat, M.Sc.</p>
        <a href="#" class="btn btn-primary">Add To Borrow</a>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card">
      <img class="card-img-top" src="{{asset('template/images/buku003.jpg')}}" alt="Card image cap">
      <div class="card-body">
        <h5 class="card-title">Filosofi Teras</h5>
        <p class="card-text">oleh Henri Manampiring</p>
        <a href="#" class="btn btn-primary">Add To Borrow</a>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card">
      <img class="card-img-top" src="{{asset('template/images/buku009.jpg')}}" alt="Card image cap">
      <div class="card-body">
        <h5 class="card-title">Melangkah</h5>
        <p class="card-text">  oleh J.S Khairen</p>
        <a href="#" class="btn btn-primary">Add To Borrow</a>
      </div>
    </div>
  </div>
</div>

@endsection
@endif
