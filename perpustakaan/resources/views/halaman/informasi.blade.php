@extends('layout.master')


@section('isi')
     <!-- Start Navigation -->
     <nav class="navbar navbar-expand-lg navbar-light bg-light navbar-default bootsnav">
        <div class="container">
            <!-- Start Header Navigation -->
            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
                <a class="navbar-brand" href="/"><img src="{{asset('template/images/gambaar.png')}}" class="logo" alt=""></a>
            </div>
            <!-- End Header Navigation -->
        <!-- Start Slider -->
    
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav ml-auto" data-in="fadeInDown" data-out="fadeOutUp">
                    <li class="nav-item"><a class="nav-link" href="{{ route('/') }}">Home</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('about') }}">About Us</a></li>
                    <li class="dropdown">
                        <a href="#" class="nav-link dropdown-toggle arrow  active" data-toggle="dropdown">Katalog</a>
                        <ul class="dropdown-menu">
                            @if (Auth::guest())
                              <li><a href="{{ route('katalog') }}">Koleksi Buku</a></li>
                              <li><a href="{{ route('lokasi') }}">Lokasi Buku</a></li>
                              <li><a href="{{ route('informasi') }}">Informasi Peminjaman</a></li>
                            @else
                              <li><a href="{{ route('master-buku.index') }}">Detail Buku</a></li>
                              <li><a href="{{ route('lokasi') }}">Lokasi Buku</a></li>
                            @endif
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="nav-link dropdown-toggle arrow" data-toggle="dropdown">Account</a>
                        <ul class="dropdown-menu">
                            @if (Auth::guest())
                                <li><a href="{{ route('login') }}">Login</a></li>
                                <li><a href="{{ route('register') }}">Register</a></li>
                            @else
                                {{ Auth::user()->name }}
                                <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                                <li><a href="{{ route('logout') }}">Logout</a></li>
                            @endif
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
    
            <!-- Start Atribute Navigation -->
    
            <!-- End Atribute Navigation -->
        </div>
        <!-- Start Side Menu -->
    
        <!-- End Side Menu -->
    </nav>
     <!-- End Navigation -->

    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Lokasi Buku</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active">Informasi Peminjaman</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <!-- Start About Page  -->
    <div class="about-box-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title-all text-center">
                        <h1>University Library</h1>
                        <h3>Cara Bergabung ke Library Leandy University</h3>
                    </div>
                </div>
          </div>
        </div>
    <div class="title-all text-center">
        <ol>
            <li>Mengunjungi Website, </li>
            <li>Mendaftar menjadi anggota di <a href="/register">Form Sign Up</a></li>
            <li>Setelah mendaftar tunggu akun anda disetujui oleh admin</li>
            <li>Kemudian anggota dapat memilih buku yang akan dipinjam</li>
            <li>Selesai</li>
        </ol>
    </div>

@endsection
