@extends('layout.master')


@section('isi')
    <!-- Start Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light navbar-default bootsnav">
        <div class="container">
            <!-- Start Header Navigation -->
            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
                <a class="navbar-brand" href="/"><img src="{{asset('template/images/gambaar.png')}}" class="logo" alt=""></a>
            </div>
            <!-- End Header Navigation -->
        <!-- Start Slider -->
    
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav ml-auto" data-in="fadeInDown" data-out="fadeOutUp">
                    <li class="nav-item"><a class="nav-link" href="{{ route('/') }}">Home</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('about') }}">About Us</a></li>
                    <li class="dropdown">
                        <a href="#" class="nav-link dropdown-toggle arrow" data-toggle="dropdown">Katalog</a>
                        <ul class="dropdown-menu">
                            @if (Auth::guest())
                              <li><a href="{{ route('katalog') }}">Koleksi Buku</a></li>
                              <li><a href="{{ route('lokasi') }}">Lokasi Buku</a></li>
                              <li><a href="{{ route('lokasi') }}">Informasi Peminjaman</a></li>
                            @else
                              <li><a href="{{ route('master-buku.index') }}">Detail Buku</a></li>
                              <li><a href="{{ route('lokasi') }}">Lokasi Buku</a></li>
                            @endif
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="nav-link dropdown-toggle arrow" data-toggle="dropdown">Account</a>
                        <ul class="dropdown-menu">
                            @if (Auth::guest())
                                <li><a href="{{ route('login') }}">Login</a></li>
                                <li><a href="{{ route('register') }}">Register</a></li>
                            @else
                                {{ Auth::user()->name }}
                                <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                                <li><a href="{{ route('logout') }}">Logout</a></li>
                            @endif
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
    
            <!-- Start Atribute Navigation -->
    
            <!-- End Atribute Navigation -->
        </div>
        <!-- Start Side Menu -->
    
        <!-- End Side Menu -->
    </nav>
    <!-- End Navigation -->
    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Shop</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active">Katalog</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <!-- Start Shop Page  -->
    <div class="shop-box-inner">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 col-lg-9 col-sm-12 col-xs-12 shop-content-right">
                    <div class="right-product-box">
                        <div class="product-item-filter row">
                            <div class="col-12 col-sm-8 text-center text-sm-left">
                                <div class="toolbar-sorter-right">
                                    <span>Sort by </span>
                                    <select id="basic" class="selectpicker show-tick form-control" data-placeholder="$ USD">
									<option data-display="Select">Nothing</option>
									<option value="1">Popularity</option>
									<option value="4">Best Selling</option>
								</select>
                                </div>
                                <p>Showing all 4 results</p>
                            </div>
                            <div class="col-12 col-sm-4 text-center text-sm-right">
                                <ul class="nav nav-tabs ml-auto">
                                    <li>
                                        <a class="nav-link active" href="#grid-view" data-toggle="tab"> <i class="fa fa-th"></i> </a>
                                    </li>
                                    <li>
                                        <a class="nav-link" href="#list-view" data-toggle="tab"> <i class="fa fa-list-ul"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="product-categorie-box">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade show active" id="grid-view">
                                    <div class="row">
                                        <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                            <div class="products-single fix">
                                                <div class="box-img-hover">
                                                    <div class="type-lb">
                                                        <p class="sale">Ready</p>
                                                    </div>
                                                    <img src="{{asset('template/images/buku001.jpg')}}" class="img-fluid" alt="Image">
                                                    <div class="mask-icon">
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="Location"><i class="fas fa-location-arrow"></i></a></li>
                                                        </ul>
                                                        <a class="cart" href="#">Add to Borrow</a>
                                                    </div>
                                                </div>
                                                <div class="why-text">
                                                    <h4>Fiersa Besari</h4>
                                                    <h5> Garis Waktu</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                            <div class="products-single fix">
                                                <div class="box-img-hover">
                                                    <div class="type-lb">
                                                        <p class="new">New</p>
                                                    </div>
                                                    <img src="{{asset('template/images/buku006.jpg')}}" class="img-fluid" alt="Image">
                                                    <div class="mask-icon">
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fas fa-location-arrow"></i></a></li>
                                                        </ul>
                                                        <a class="cart" href="/login">Add to Borrow</a>
                                                    </div>
                                                </div>
                                                <div class="why-text">
                                                    <h4>Mommy Asf</h4>
                                                    <h5> Layangan Putus</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                            <div class="products-single fix">
                                                <div class="box-img-hover">
                                                    <div class="type-lb">
                                                        <p class="sale">Sale</p>
                                                    </div>
                                                    <img src="{{asset('template/images/buku003.jpg')}}" class="img-fluid" alt="Image">
                                                    <div class="mask-icon">
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fas fa-location-arrow"></i></a></li>
                                                        </ul>
                                                        <a class="cart" href="/login">Add to Borrow</a>
                                                    </div>
                                                </div>
                                                <div class="why-text">
                                                    <h4>Henry Manmpiring</h4>
                                                    <h5> Filosofi Teras</h5> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                            <div class="products-single fix">
                                                <div class="box-img-hover">
                                                    <div class="type-lb">
                                                        <p class="new">New</p>
                                                    </div>
                                                    <img src="{{asset('template/images/buku005.jpg')}}" class="img-fluid" alt="Image">
                                                    <div class="mask-icon">
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fas fa-location-arrow"></i></a></li>
                                                        </ul>
                                                        <a class="cart" href="/login">Add to Borrow</a>
                                                    </div>
                                                </div>
                                                <div class="why-text">
                                                    <h4>Andrea Hirata</h4>
                                                    <h5> Guru Aini</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                            <div class="products-single fix">
                                                <div class="box-img-hover">
                                                    <div class="type-lb">
                                                        <p class="sale">Sale</p>
                                                    </div>
                                                    <img src="{{asset('template/images/buku004.jpg')}}" class="img-fluid" alt="Image">
                                                    <div class="mask-icon">
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fas fa-location-arrow"></i></a></li>
                                                        </ul>
                                                        <a class="cart" href="#">Add to Borrow</a>
                                                    </div>
                                                </div>
                                                <div class="why-text">
                                                    <h4>Nanti Kita Cerita Tentang Hari ini</h4>
                                                    <h5> Marcella FP</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                            <div class="products-single fix">
                                                <div class="box-img-hover">
                                                    <div class="type-lb">
                                                        <p class="sale">Sale</p>
                                                    </div>
                                                    <img src="{{asset('template/images/buku002.jpg')}}" class="img-fluid" alt="Image">
                                                    <div class="mask-icon">
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fas fa-location-arrow"></i></a></li>
                                                        </ul>
                                                        <a class="cart" href="/login">Add to Borrow</a>
                                                    </div>
                                                </div>
                                                <div class="why-text">
                                                    <h4>Jika Kita Tak Pernah Terjadi Apa-Apa</h4>
                                                    <h5> Alvi Syahrin</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                            <div class="products-single fix">
                                                <div class="box-img-hover">
                                                    <div class="type-lb">
                                                        <p class="sale">Sale</p>
                                                    </div>
                                                    <img src="{{asset('template/images/buku009.jpg')}}" class="img-fluid" alt="Image">
                                                    <div class="mask-icon">
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fas fa-location-arrow"></i></a></li>
                                                        </ul>
                                                        <a class="cart" href="#">Add to Borrow</a>
                                                    </div>
                                                </div>
                                                <div class="why-text">
                                                    <h4>Melangkah</h4>
                                                    <h5> JS Khairen</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                            <div class="products-single fix">
                                                <div class="box-img-hover">
                                                    <div class="type-lb">
                                                        <p class="sale">Sale</p>
                                                    </div>
                                                    <img src="{{asset('template/images/buku010.jpg')}}" class="img-fluid" alt="Image">
                                                    <div class="mask-icon">
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fas fa-location-arrow"></i></a></li>
                                                        </ul>
                                                        <a class="cart" href="#">Add to Borrow</a>
                                                    </div>
                                                </div>
                                                <div class="why-text">
                                                    <h4>Selamat Tinggal</h4>
                                                    <h5> Tere Liye</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                            <div class="products-single fix">
                                                <div class="box-img-hover">
                                                    <div class="type-lb">
                                                        <p class="new">New</p>
                                                    </div>
                                                    <img src="{{asset('template/images/buku008.jpg')}}" class="img-fluid" alt="Image">
                                                    <div class="mask-icon">
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fas fa-location-arrow"></i></a></li>
                                                        </ul>
                                                        <a class="cart" href="/login">Add to Borrow</a>
                                                    </div>
                                                </div>
                                                <div class="why-text">
                                                    <h4>Psikologi Komunikasi</h4>
                                                    <h5> Drs. Jalaluddudin Rakhmat, M.Sc. </h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="list-view">
                                    <div class="list-view-box">
                                        <div class="row">
                                            <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                                <div class="products-single fix">
                                                    <div class="box-img-hover">
                                                        <div class="type-lb">
                                                            <p class="new">New</p>
                                                        </div>
                                                        <img src="{{asset('template/images/buku009.jpg')}}" class="img-fluid" alt="Image">
                                                        <div class="mask-icon">
                                                            <ul>
                                                                <li><a href="#" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                                                <li><a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fas fa-location-arrow"></i></a></li>
                                                            </ul>
                                                            <a class="cart" href="#">Add to Borrow</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-8 col-xl-8">
                                                <div class="why-text full-width">
                                                    <h4>Melangkah (JS Khairen)</h4>
                                                    <h5>  Gramedia </h5>
                                                    <p>Listrik padam di seluruh Jawa dan Bali secara misterius! Ancaman nyata kekuatan baru yang hendak menaklukkan Nusantara. Saat yang sama, empat sahabat mendarat di Sumba, hanya untuk mendapati nasib ratusan juta manusia ada di tangan mereka! Empat mahasiswa ekonomi ini, harus bertarung melawan pasukan berkuda yang bisa melontarkan listrik! Semua dipersulit oleh seorang buronan tingkat tinggi bertopeng pahlawan yang punya rencana mengerikan.
                                                        Ternyata pesan arwah nenek moyang itu benar-benar terwujud. “Akan datang kegelapan yang berderap, bersama ribuan kuda raksasa di kala malam. Mereka bangun setelah sekian lama, untuk menghancurkan Nusantara. Seorang lelaki dan seorang perempuan ditakdirkan membaurkan air di lautan dan api di pegunungan. Menyatukan tanah yang menghujam, dan udara yang terhampar.”.
                                                        Kisah tentang persahabatan, tentang jurang ego anak dan orangtua, tentang menyeimbangkan logika dan perasaan. Juga tentang melangkah menuju masa depan. Bahwa, apa pun yang menjadi luka masa lalu, biarlah mengering bersama waktu.</p>
                                                    <a class="btn hvr-hover" href="#">Add to Cart</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-view-box">
                                        <div class="row">
                                            <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                                <div class="products-single fix">
                                                    <div class="box-img-hover">
                                                        <div class="type-lb">
                                                            <p class="sale">Ready</p>
                                                        </div>
                                                        <img src="{{asset('template/images/buku004.jpg')}}" class="img-fluid" alt="Image">
                                                        <div class="mask-icon">
                                                            <ul>
                                                                <li><a href="#" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                                                <li><a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fas fa-location-arrow"></i></a></li>
                                                                <li><a href="#" data-toggle="tooltip" data-placement="right" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                                            </ul>
                                                            <a class="cart" href="#">Add to Borrow</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-8 col-xl-8">
                                                <div class="why-text full-width">
                                                    <h4>Nanti Kita Cerita Tentang Hari Ini (Marcella FP)</h4>
                                                    <h5>  Gramedia or Rak Buku Kode 009 </h5>
                                                    <p>Di buku ini kita bertemu dengan seorang perempuan berusia 27 tahun dan juga seorang ibu. Perempuan di buku itu bernama Awan. Nama itu dipilih karena “awan selalu punya cara untuk menjaga dan menghibur bumi serta isinya,” ujar sang ibu.
                                                        Namun tak mudah menjadi awan seperti harapan ibunya. Baginya, jadi bohlam saja lebih dari cukup, untuk menerangi dan memberi kehangatan sebuah ruang yang kecil.           
                                                        Awan lalu menulis surat untuk masa depan. Surat yang ditujukan untuk anaknya. Surat yang berisi “Tentang memori, gagal, tumbuh, patah, bangun, hilang, menunggu, bertahan, berubah, dan semua ketakutan manusia pada umumnya.”</p>
                                                    <a class="btn hvr-hover" href="#">Add to Cart</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-view-box">
                                        <div class="row">
                                            <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                                <div class="products-single fix">
                                                    <div class="box-img-hover">
                                                        <div class="type-lb">
                                                            <p class="sale">Ready</p>
                                                        </div>
                                                        <img src="{{asset('template/images/buku006.jpg')}}" class="img-fluid" alt="Image">
                                                        <div class="mask-icon">
                                                            <ul>
                                                                <li><a href="#" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                                                <li><a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fas fa-location-arrow"></i></a></li>
                                                                <li><a href="#" data-toggle="tooltip" data-placement="right" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                                            </ul>
                                                            <a class="cart" href="#">Add to Borrow</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-8 col-xl-8">
                                                <div class="why-text full-width">
                                                    <h4>Layangan Putus (Mommy As)</h4>
                                                    <h5> Gramedia or Rak Buku Kode 008</h5>
                                                    <p>Seorang gadis remaja polos yang berasal dari daerah, tumbuh, berkembang, dan menemukan cinta di kota besar yang sangat berbeda dengan iklim daerah asalnya. 
                                                        Mimpi sederhananya menyambung pendidikan dan menyelesaikannya tepat waktu, namun berubah setelah ia mengenal sosok lelaki tangguh Lelaki yang mandiri dan berpendirian keras mengenalkannya dengan dunia baru yang belum pernah ia temui. Dunia yang asyik dan menyenangkan yang berbeda total dengan kehidupan remaja di daerah asalnya. Kinan jatuh cinta dengan sosok fun Aris yang juga memiliki sifat gigih. Aris mengubah caranya memandang dunia. Berdua menyamakan visi dan berjanji dalam ikatan pernikahan. Bersama memulai semua kehidupan dari bawah, Kinan dengan setia mendampingi Aris membangun mimpi mereka. Perubahan pola pikir Aris kembali mengubah cara pandang Kinan terhadap prioritas kehidupan. 
                                                        Kinan tetap setia di sisiAris dan melupakan mimpinya menjadi seorang wanita karier. Memilih merawat keluarga di rumah memenuhi permintaan Aris Dan kembali mengenal Tuhan. Aris mampu meyakinkan Kinan dengan cukup ia yang bekerja di luar rumah, sudah cukup membawa Kinan, memenuhi mimpinya saat kecil bertualang menaiki balon udara.
                                                        Kinan jatuh cinta akan keindahan Cappadocia dan balon udara yang menghiasi angkasa. Takdir berkata Iain. Aris menyaksikan keindahan tersebut namun Kinan hanya sanggup menikmati dari foto-foto yang ia temukan di ponsel suaminya. Bersama wanita Iain. Dua belas hari menghilang, Aris kembali ke rumah dengan semua hal baru yang belum pernah diketahui Kinan. Akankah Aris kembali berhasill membuat Kinan mengerti akan pilihannya.</p>
                                                    <a class="btn hvr-hover" href="#">Add to Cart</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="col-xl-3 col-lg-3 col-sm-12 col-xs-12 sidebar-shop-left">
                    <div class="product-categori">
                        <div class="search-product">
                            <form action="#">
                                <input class="form-control" placeholder="Search here..." type="text">
                                <button type="submit"> <i class="fa fa-search"></i> </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Shop Page -->
@endsection