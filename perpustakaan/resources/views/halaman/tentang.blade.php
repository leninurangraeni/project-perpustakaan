@extends('layout.master')


@section('isi')
     <!-- Start Navigation -->
     @include('navbar.nav')
     <!-- End Navigation -->

    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>ABOUT US</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active">ABOUT US</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <!-- Start About Page  -->
    <div class="about-box-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title-all text-center">
                        <h1>University Library</h1>
                    </div>
                </div>
            </div>
            <div class="row my-5">
                <div class="col-sm-6 col-lg-4">
                    <div class="service-block-inner">
                        <h3>We are Trusted</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="service-block-inner">
                        <h3>We are Professional</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="service-block-inner">
                        <h3>We are Expert</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>
                </div>
            </div>
            <div class="row my-4">
                <div class="col-12">
                    <h2 class="noo-sh-title">Meet Our Team</h2>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="hover-team">
                        <div class="our-team"> <img src="{{asset('template/images/orang1.png')}}" alt="" />
                            <div class="team-content">
                                <h3 class="title">Leni</h3> <span class="post">Pustakawan</span> </div>
                            <ul class="social">
                                <li>
                                    <a href="https://m.facebook.com/leni.nurangraeni/" class="fab fa-facebook"></a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/leni_angraenii" class="fab fa-twitter"></a>
                                </li>
                            </ul>
                            <div class="icon"> <i class="fa fa-plus" aria-hidden="true"></i> </div>
                        </div>
                        <div class="team-description">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                        </div>
                        <hr class="my-0"> </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="hover-team">
                        <div class="our-team"> <img src="{{asset('template/images/orang2.png')}}" alt="" />
                            <div class="team-content">
                                <h3 class="title">Angraeni</h3> <span class="post">Web Developer</span> </div>
                            <ul class="social">
                                <li>
                                    <a href="https://m.facebook.com/leni.nurangraeni/" class="fab fa-facebook"></a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/leni_angraenii" class="fab fa-twitter"></a>
                                </li>
                            </ul>
                            <div class="icon"> <i class="fa fa-plus" aria-hidden="true"></i> </div>
                        </div>
                        <div class="team-description">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                        <hr class="my-0"> </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End About Page -->
    <!-- Start Contact Us  -->
      <div class="contact-box-main">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-sm-12">
                        <div class="contact-form-right">
                            <h2>CONTACT US</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed odio justo, ultrices ac nisl sed, lobortis porta elit. Fusce in metus ac ex venenatis ultricies at cursus mauris.</p>
                            <ul>
                                <li>
                                    <p><i class="fas fa-map-marker-alt"></i>  Address: Jalan Pemuda Nomor 047 <br>Desa Panawuan, Kecamatan Cigandamekar, Kabupaten Kuningan, Provinsi Jawa Barat<br> Kode Post 4556 </p>
                                </li>
                                <li>
                                    <p><i class="fas fa-phone-square"></i>  Phone: <a href="tel:089606154935">089606154935</a></p>
                                </li>
                                <li>
                                    <p><i class="fas fa-envelope"></i>  Email: <a href="leninurangraeni@gmail.com">leninurangraeni@gmail.com</a></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- End Cart -->
    @endsection