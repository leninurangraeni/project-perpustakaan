@extends('layout.master')


@section('isi')
    <!-- Start Navigation -->
    @include('navbar.depan')
    <!-- End Navigation -->

    <!-- Start Slider -->
    @include('partials.slide')
    <!-- End Slider -->

    <!-- Start Categories  -->
    <div class="categories-shop">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="shop-cat-box">
                        <img class="img-fluid" src="{{asset('template/images/kategori001.jpg')}}" alt="" />
                        <a class="btn hvr-hover" href="/katalog">Katalog</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="shop-cat-box">
                        <img class="img-fluid" src="{{asset('template/images/kategori002.jpg')}}" alt="" />
                        <a class="btn hvr-hover" href="/lokasi">Lokasi Buku</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="shop-cat-box">
                        <img class="img-fluid" src="{{asset('template/images/kategori003.jpg')}}" alt="" />
                        <a class="btn hvr-hover" href="/informasi">Informasi Peminjaman</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Categories -->

    <!-- Start Products  -->
    <div class="products-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title-all text-center">
                        <h1>Koleksi Buku</h1>
                        <p>Berikut ini adalah beberapa koleksi yang ada di perpustakaan.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="special-menu text-center">
                        <div class="button-group filter-button-group">
                            <button class="active" data-filter="*">All</button>
                            <button data-filter=".top-featured">Top featured</button>
                            <button data-filter=".best-seller">Best seller</button>
                        </div>
                    </div>
                </div>
            </div>
    
            <div class="row special-list">
                <div class="col-lg-3 col-md-6 special-grid best-seller">
                    <div class="products-single fix">
                        <div class="box-img-hover">
                            <div class="type-lb">
                                <p class="sale">Ready</p>
                            </div>
                            <img src="{{asset('template/images/buku001.jpg')}}" class="img-fluid" alt="Image">
                            <div class="mask-icon">
                                <ul>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="Location"><i class="fas fa-location-arrow"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                </ul>
                                <a class="cart" href="/login">Add to Borrow</a>
                            </div>
                        </div>
                        <div class="why-text">
                            <h4>Fiersa Besari</h4>
                            <h5> Garis Waktu</h5>
                        </div>
                    </div>
                </div>
    
                <div class="col-lg-3 col-md-6 special-grid best-seller">
                    <div class="products-single fix">
                        <div class="box-img-hover">
                            <div class="type-lb">
                                <p class="new">New</p>
                            </div>
                            <img src="{{asset('template/images/buku006.jpg')}}" class="img-fluid" alt="Image">
                            <div class="mask-icon">
                                <ul>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fas fa-location-arrow"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                </ul>
                                <a class="cart" href="/login">Add to Borrow</a>
                            </div>
                        </div>
                        <div class="why-text">
                            <h4>Mommy Asf</h4>
                            <h5> Layangan Putus</h5>
                        </div>
                    </div>
                </div>
    
                <div class="col-lg-3 col-md-6 special-grid top-featured">
                    <div class="products-single fix">
                        <div class="box-img-hover">
                            <div class="type-lb">
                                <p class="sale">Ready</p>
                            </div>
                            <img src="{{asset('template/images/buku003.jpg')}}" class="img-fluid" alt="Image">
                            <div class="mask-icon">
                                <ul>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fas fa-location-arrow"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                </ul>
                                <a class="cart" href="/login">Add to Borrow</a>
                            </div>
                        </div>
                        <div class="why-text">
                            <h4>Henry Manmpiring</h4>
                            <h5> Filosofi Teras</h5>
                        </div>
                    </div>
                </div>
    
                <div class="col-lg-3 col-md-6 special-grid top-featured">
                    <div class="products-single fix">
                        <div class="box-img-hover">
                            <div class="type-lb">
                                <p class="sale">Ready</p>
                            </div>
                            <img src="{{asset('template/images/buku005.jpg')}}" class="img-fluid" alt="Image">
                            <div class="mask-icon">
                                <ul>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fas fa-location-arrow"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                </ul>
                                <a class="cart" href="/login">Add to Borrow</a>
                            </div>
                        </div>
                        <div class="why-text">
                            <h4>Andrea Hirata</h4>
                            <h5> Guru Aini</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
    <!-- Start Products  -->

@endsection