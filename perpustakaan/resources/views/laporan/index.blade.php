@extends('layout.dasbor')
 
@section('judul')
Data Laporan Peminjaman
@endsection

@section('konten')

<button type="submit" value="Refresh" class="btn btn-secondary btn-sm mb-3"><i class="fas fa-sync" onClick="document.location.reload(true)">             Refresh</i></button>
@if (session('best'))
    <div class="alert alert-success">
      {{ session('best') }}
    </div>
@endif

<div class="card-body table-responsive p-0" style="height: 300px;">
   <table class="table">
        <thead class="table-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">User</th>
                <th scope="col">Buku</th>
                <th scope="col">Jenis Transaksi</th>
                <th scope="col">Tanggal Pengajuan</th>
                <th scope="col">Status</th>
                <th scope="col">Tanggal Pengembalian</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($datalaporan as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->user_id}}</td>
                <td>{{$item->judul}}</td>
                <td>{{$item->jenis_transaksi}}</td>
                <td>{{$item->tanggal_pengajuan}}</td>
                <td>{{$item->nama}}</td>
                @if ($item->status ==3)
                <td>{{$item->tanggal_pengembalian}}</td>
                @else
                <td></td>
                @endif
            </tr>
            @endforeach
      </tbody>
 </table>
</div>
@endsection