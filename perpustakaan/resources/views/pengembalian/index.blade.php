@extends('layout.dasbor')
 
@section('konten')

<button type="submit" value="Refresh" class="btn btn-secondary btn-sm mb-3"><i class="fas fa-sync" onClick="document.location.reload(true)">             Refresh</i></button>
@if (session('best'))
    <div class="alert alert-success">
      {{ session('best') }}
    </div>
@endif

<div class="card-body table-responsive p-0" style="height: 300px;">
   <table class="table">
        <thead class="table-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">User</th>
                <th scope="col">Buku</th>
                <th scope="col">Jenis Transaksi</th>
                <th scope="col">Tanggal Pengajuan</th>
                <th scope="col">Status</th>
                <th scope="col">Tanggal Pengembalian</th>
                @if(Auth::user()->roles_id == 1)
                <th scope="col">Action</th>
                @endif
            </tr>
        </thead>
        <tbody>
          @forelse ($pinjamdata as $key => $item)
              <tr>
                  @if( Auth::user()->id)
                  <td>{{$key + 1}}</td>
                  <td>{{$item->user_id}}</td>
                  <td>{{$item->judul}}</td>
                  <td>{{$item->jenis_transaksi}}</td>
                  <td>{{$item->tanggal_pengajuan}}</td>
                  @if ($item->status == 1)
                  <td><span class="bg-success">Sedang Di Pinjam</span></td>
                  @else 
                  <td><span class="bg-primary">Sudah Di Kembalikan</span></td>
                  @endif
                  @if ($item->status ==3)
                  <td>{{$item->tanggal_pengembalian}}</td>
                  @else
                  <td></td>
                  @endif
                  @if(Auth::user()->roles_id == 1)
                  @if ($item->status ==1)
                  <td>
                    <a href="{{ route('pengembalian-buku.kembalikan',$item->id) }}" class="btn btn-primary btn-sm">Kembalikan</a>
                  </td>
                  @else
                  <td>
                    <form action="" method="POST">
                        @csrf
                        <a href="{{ route('pengembalian-buku.edit',$item->id) }}" class="btn btn-warning btn-sm"><i class="fas fa-pen"></i></a>
                  </form>
                  </td>
                  @endif
                  @endif
                  @endif
              </tr>
          @empty
               <tr>
                  <td>Data Masih Kosong</td>
              </tr>
          @endforelse
      </tbody>
 </table>
</div>
@endsection