@extends('layout.dasbor')
@section('judul')

@endsection

@section('konten')
  <form action="{{ route('pengembalian-buku.update') }}" method="POST">
      @csrf
      @method('put')
      <div class="form-group">
        <label >Tanggal Pengembalian</label>
        <input type="date" name="tanggal_pengembalian" class="form-control">
      </div>
      @error('tanggal_pengembalian')
         <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <button type="submit" class="btn btn-primary">Update</button>
      @error('tanggal_pengembalian')
       <div class="alert alert-danger">{{ $message }}</div>
      @enderror
  </form>
@endsection
