@extends('layout.dasbor')
 
@section('konten')

<a href="{{ route('pinjam-buku.form') }}" class="btn btn-primary btn-sm mb-3"><i class="fas fa-plus"></i>        Formulir peminjaman</a>
<button type="submit" value="Refresh" class="btn btn-secondary btn-sm mb-3"><i class="fas fa-sync" onClick="document.location.reload(true)">             Refresh</i></button>
@if (session('best'))
    <div class="alert alert-success">
      {{ session('best') }}
    </div>
@endif

<div class="card-body table-responsive p-0" style="height: 300px;">
  <table class="table">
    <thead class="table-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">User</th>
        <th scope="col">Buku</th>
        <th scope="col">Jenis Transaksi</th>
        <th scope="col">Tanggal Pengajuan</th>
        <th scope="col">Status</th>
        @if(Auth::user()->roles_id == 1)
        <th scope="col">Action</th>
        @endif
      </tr>
    </thead>
    <tbody>
        @forelse ($pinjamdata as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->user_id}}</td>
                <td>{{$item->judul}}</td>
                <td>{{$item->jenis_transaksi}}</td>
                <td>{{$item->tanggal_pengajuan}}</td>
                @if ($item->status == null)
                <td><span class="bg-warning">Menunggu Verifikasi</span></td>
                @elseif($item->status == 1)
                <td><span class="bg-success">Disetujui</span></td>
                @elseif($item->status == 2)
                <td><span class="bg-danger">Ditolak</span></td>
                @endif
                @if(Auth::user()->roles_id == 1)
                @if ($item->status == null)
                <td>
                  <a href="{{ route('pinjam-buku.setujui',$item->id) }}" class="btn btn-primary btn-sm"><i class="fas fa-check"></i>       Setujui</a>
                  <a href="{{ route('pinjam-buku.tolak',$item->id) }}" class="btn btn-danger btn-sm">Tolak</a>
                </td>
                @elseif ($item->status == 1)
                <td>
                  <a href="{{ route('pinjam-buku.tolak',$item->id) }}" class="btn btn-danger btn-sm">Tolak</a>
                </td>
                @elseif ($item->status == 2) 
                <td>
                  <a href="{{ route('pinjam-buku.setujui',$item->id) }}" class="btn btn-primary btn-sm"><i class="fas fa-check"></i>       Setujui</a>
                </td>
                @endif
                @endif
            </tr>
        @empty
          <tr>
              <td>Data Masih Kosong</td>
          </tr>
        @endforelse
    </tbody>
  </table>
</div>
@endsection

@section('scripts')
  <script type="text/javascript">
      $(document).ready(function(){
          // btn refresh 
            $('.btn-refresh').click(function(e){
                e.preventDefault();
                $('.preloader').fadeIn();
                location.reload();
            })
       })
  </script>
@endsection 



