@extends('layout.dasbor')
 
@section('judul')

Formulir Peminjaman

@endsection

@section('konten')
<p>
    <a href="javascript:history.back()" class="btn btn-sm btn-flat btn-warning">Back</a>
</p>
  <form action="{{ route('pinjam-buku.save') }}" method="post">
    @csrf
    <div class="form-group">
      <label >Nama</label>
      <input type="text" value="{{Auth::user()->id}}" name="user_id" class="form-control">
    </div>
    @error('user_id')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <select class="form-control" name="buku_id" id="exampleRepeatPassword"  required="">
          <option selected="" disabled>Pilih Buku</option>
          @foreach ($ambilbuku as $item)
          <option value="{{ $item->id }}">{{ $item->judul }}</option>
          @endforeach
      </select>
    </div>
    @error('buku_id')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Jenis Transaksi</label>
      <input type="text" name="jenis_transaksi" class="form-control">
    </div>
    @error('jenis_transaksi')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label for="exampleInputTanggalPengajuan">Tanggal Pengajuan</label>
      <input type="date" name="tanggal_pengajuan" class="form-control">
    </div>
    <div class="form-group">
      <label >Tanggal Pengembalian</label>
      <input type="date" name="tanggal_pengembalian" class="form-control">
    </div>
    @error('tanggal_pengembalian')
       <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Status</label>
      <input type="text" name="status" class="form-control">
    </div>
    @error('status')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
    @error('tanggal_pengajuan')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </form>


@endsection
 
 