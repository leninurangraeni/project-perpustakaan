@extends('layout.dasbor')
 
 @section('konten')
 <h1>{{$databuku->judul}}</h1>
 <p>{{$databuku->deskripsi}}</p>
 <p>{{$databuku->pengarang}}</p>
 <p>{{$databuku->tahun}}</p>
 <p>{{$databuku->kode_buku}}</p>
 <p>{{$databuku->stok_buku}}</p>
 <p>{{$databuku->lokasi}}</p>
 <p>{{$databuku->status_buku}}</p>

 <a href="{{ route('master-buku.index') }}" class="btn btn-secondary">Back</a>

 @endsection