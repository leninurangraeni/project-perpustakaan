@extends('layout.dasbor')
@section('judul')
Edit Data Buku {{$databuku->judul}}
@endsection

@section('konten')
  <form action="{{ route('master-buku.update',$databuku->id) }}" method="POST">
      @csrf
      @method('put')
      <div class="form-group">
        <label >Judul</label>
        <input type="text" value="{{$databuku->judul}}" name="judul" class="form-control">
      </div>
      @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label >Deskripsi</label>
        <textarea value="{{$databuku->deskripsi}}" name="deskripsi" cols="30" rows="10" class="form-control"></textarea>
      </div>
      @error('email')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label >Pengarang</label>
        <input type="text" value="{{$databuku->pengarang}}" name="pengarang" class="form-control">
      </div>
      <div class="form-group">
        <label >Tahun</label>
        <input type="number" value="{{$databuku->tahun}}" name="tahun" class="form-control">
      </div>
      @error('tahun')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label >Kode Buku</label>
        <input type="number" value="{{$databuku->kode_buku}}" name="kode_buku" class="form-control">
      </div>
      @error('kode_buku')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label >Stok Buku</label>
        <input type="number" value="{{$databuku->stok_buku}}" name="stok_buku" class="form-control">
      </div>
      @error('stok_buku')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label >Lokasi Buku</label>
        <input type="teks" value="{{$databuku->lokasi}}" name="lokasi" class="form-control">
      </div>
      @error('lokasi')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label >Status</label>
        <input type="number" value="{{$databuku->status_buku}}" name="status_buku" class="form-control">
      </div>
      @error('status')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <button type="submit" class="btn btn-primary">Update</button>
      @error('pengarang')
       <div class="alert alert-danger">{{ $message }}</div>
      @enderror
  </form>

@endsection
