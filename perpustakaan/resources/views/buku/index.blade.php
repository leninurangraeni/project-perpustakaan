@extends('layout.dasbor')
 
@section('judul')
Data Buku
@endsection

@section('konten')
@if(Auth::user()->roles_id == 1)
<a href="{{ route('master-buku.create') }}" class="btn btn-primary btn-sm mb-3"><i class="fas fa-plus"></i>         Tambah Buku</a>
@endif
<button type="submit" value="Refresh" class="btn btn-light btn-sm mb-3"><i class="fas fa-sync" onClick="document.location.reload(true)">             Refresh</i></button>
<a href="{{ route('master-buku.index') }}" class="btn btn-success btn-sm mb-3"> All Buku</a>
<a href="{{ route('master-buku.blank') }}" class="btn btn-danger btn-sm mb-3"> Stok Buku Habis</a>
<a href="{{ route('master-buku.nonaktif') }}" class="btn btn-info btn-sm mb-3"> Buku Nonaktif</a>
@if (session('success'))
    <div class="alert alert-success">
      {{ session('success') }}
    </div>
@endif

<div class="card-body table-responsive p-0" style="height: 300px;">
  <table class="table">
    <thead class="table-dark">
      <tr>
        <th scope="col">#</th>
        @if(Auth::user()->roles_id == 1)
        <th scope="col">Keadaan</th>
        <th scope="col">Tindak</th>
        @endif
        <th scope="col">Pinjam</th>
        <th scope="col">Judul</th>
        <th scope="col">Deskripsi</th>
        <th scope="col">Pengarang</th>
        <th scope="col">Tahun</th>
        <th scope="col">Kode_Buku</th>
        <th scope="col">Stok_Buku</th>
        <th scope="col">Lokasi</th>
        <th scope="col">Status</th>
        @if(Auth::user()->roles_id == 1)
        <th scope="col">Action</th>
        @endif
      </tr>
    </thead>
    <tbody>
        @forelse ($databuku as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>

                @if(Auth::user()->roles_id == 1)
                <td>
                  @if($item->status_buku == 1)
                  <a href="{{ route('master-buku.status',$item->id) }}" class="btn btn-danger btn-sm">Non-Aktifkan</a>
                  @else
                  <a href="{{ route('master-buku.status',$item->id) }}" class="btn btn-success btn-sm">Aktifkan</a>
                  @endif
                </td>
                <td>
                  <a href="{{ route('master-buku.ambil',$item->id) }}" class="btn btn-primary btn-sm">Kurangi Stok</a>
                </td>
                @endif

                <td>
                  <a href="{{ route('pinjam-buku.cek',$item->id) }}" class="btn btn-warning btn-sm">Pinjam Buku</a>
                </td>
                <td>{{$item->judul}}</td>
                <td>{{$item->deskripsi}}</td>
                <td>{{$item->pengarang}}</td>
                <td>{{$item->tahun}}</td>
                <td>{{$item->kode_buku}}</td>
                <td>{{$item->stok_buku}}</td>
                <td>{{$item->lokasi}}</td>
                <td><span class="badge {{ ($item->status_buku == 1) ? 'bg-success' : 'bg-danger' }}">{{ ($item->status_buku == 1) ? 'Aktif' : 'Tidak Aktif' }}</span></td>
                @if(Auth::user()->roles_id == 1)
                <td>
                  
                    <form action="{{ route('master-buku.destroy',$item->id) }}" method="POST">
                          @csrf
                          @method('delete')
                          <a href="{{ route('master-buku.show',$item->id) }}" class="btn btn-info btn-sm"><i class="fas fa-eye"></i></a>
                          <a href="{{ route('master-buku.edit',$item->id) }}" class="btn btn-warning btn-sm"><i class="fas fa-pen"></i></a>
                          <button type="submit" value="delete" class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i></button>

                    </form>
                </td>
                @endif
            </tr>
        @empty
          <tr>
              <td>Data Masih Kosong</td>
          </tr>
        @endforelse
    </tbody>
  </table>
</div>
@endsection
