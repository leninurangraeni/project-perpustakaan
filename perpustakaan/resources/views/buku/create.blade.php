@extends('layout.dasbor')
 
@section('judul')

Tambah Buku
@endsection

@section('konten')
<p>
    <a href="javascript:history.back()" class="btn btn-sm btn-flat btn-warning">Back</a>
</p>
  <form action="{{ route('master-buku.load') }}" method="post">
    @csrf
    <div class="form-group">
      <label >Judul Buku</label>
      <input type="text" name="judul" class="form-control">
    </div>
    @error('judul')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label for="exampleInputDeskripsi">Deskripsi</label>
      <textarea name="deskripsi" cols="30" rows="10" class="form-control"></textarea>
    </div>
    <div class="form-group">
      <label >Pengarang</label>
      <input type="text" name="pengarang" class="form-control">
    </div>
    @error('pengarang')
       <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Tahun</label>
      <input type="text" name="tahun" class="form-control">
    </div>
    @error('tahun')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Kode Buku</label>
      <input type="number" name="kode_buku" class="form-control">
    </div>
    @error('kode_buku')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Stok Buku</label>
      <input type="number" name="stok_buku" class="form-control">
    </div>
    @error('stok_buku')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Lokasi Buku</label>
      <input type="text" name="lokasi" class="form-control">
    </div>
    @error('lokasi')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror    <div class="form-group">
    <div class="form-group">
      <label >Status</label>
      <input type="number" name="status" class="form-control">
    </div>
    @error('status')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
    @error('deskripsi')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </form>

@endsection
 
 