@extends('layout.dasbor')
 
@section('konten')

<a href="{{ route('roles.add') }}" class="btn btn-primary btn-sm mb-3"><i class="fas fa-plus"></i>         Tambah Roles</a>
<button type="submit" value="Refresh" class="btn btn-secondary btn-sm mb-3"><i class="fas fa-sync" onClick="document.location.reload(true)">             Refresh</i></button>
@if (session('best'))
    <div class="alert alert-success">
      {{ session('best') }}
    </div>
@endif

<table class="table">
  <thead class="table-dark">
    <tr>
      <th scope="col md-8 col-md-offset-2">#</th>
      <th scope="col md-8 col-md-offset-2">Roles</th>
      <th scope="col">Created_at</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
      @forelse ($rolesdata as $key => $item)
          <tr>
              <td>{{$key + 1}}</td>
              <td>{{$item->peran}}</td>
              <td>{{$item->created_at}}</td>
              <td>

                  <form action="{{ route('roles.destroy',$item->id) }}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="{{ route('roles.edit',$item->id) }}" class="btn btn-warning btn-sm"><i class="fas fa-pen"></i></a>
                        <button type="submit" value="delete" class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i></button>
                  </form>
              </td>
          </tr>
      @empty
         <tr>
             <td>Data Masih Kosong</td>
         </tr>
      @endforelse
  </tbody>
</table>
@endsection

@section('scripts')
  <script type="text/javascript">
      $(document).ready(function(){
          // btn refresh 
            $('.btn-refresh').click(function(e){
                e.preventDefault();
                $('.preloader').fadeIn();
                location.reload();
            })
       })
  </script>
@endsection 



