@extends('layout.dasbor')
 
@section('judul')

Tambah Roles
@endsection

@section('konten')
<p>
    <a href="javascript:history.back()" class="btn btn-sm btn-flat btn-warning">Back</a>
</p>
  <form action="{{ route('roles.store') }}" method="POST">
    @csrf
    <div class="form-group">
      <label >Nama Roles</label>
      <input type="text" name="peran" class="form-control">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    @error('peran')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </form>


@endsection
 
 