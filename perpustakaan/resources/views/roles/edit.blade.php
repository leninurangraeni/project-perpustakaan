@extends('layout.dasbor')
@section('judul')
Edit Roles {{$rolesdata->roles}}
@endsection

@section('konten')
  <form action="{{ route('roles.update',$rolesdata->id) }}" method="POST">
      @csrf
      @method('put')
      <div class="form-group">
        <label >Nama Anggota</label>
        <input type="text" value="{{$rolesdata->roles}}" name="peran" class="form-control">
      </div>
      <button type="submit" class="btn btn-primary">Update</button>
      @error('peran')
       <div class="alert alert-danger">{{ $message }}</div>
      @enderror
  </form>
@endsection
