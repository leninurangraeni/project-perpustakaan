@extends('layout.dasbor')
 
@section('konten')

<a href="{{ route('manage-akun.create') }}" class="btn btn-primary btn-sm mb-3"><i class="fas fa-plus"></i>         Tambah Anggota</a>
<button type="submit" value="Refresh" class="btn btn-secondary btn-sm mb-3"><i class="fas fa-sync" onClick="document.location.reload(true)">             Refresh</i></button>
@if (session('success'))
    <div class="alert alert-success">
      {{ session('success') }}
    </div>
@endif

<div class="card-body table-responsive p-0" style="height: 300px;">
<table class="table">
  <thead class="table-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Keadaan</th>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">Status</th>
      <th scope="col">Profil</th>
      <th scope="col">NIK</th>
      <th scope="col">Roles_id</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
      @forelse ($userdata as $key => $item)
          <tr>
              <td>{{$key + 1}}</td>
              <td>
                @if($item->status_akun == 1)
                <a href="{{ route('manage-akun.status',$item->id) }}" class="btn btn-danger btn-sm">Batalkan Disetujui</a>
                @else
                <a href="{{ route('manage-akun.status',$item->id) }}" class="btn btn-success btn-sm">Setujui</a>
                @endif 
              </td>
              <td>{{$item->name}}</td>
              <td>{{$item->email}}</td>
              <td><span class="badge {{ ($item->status_akun == 1) ? 'bg-success' : 'bg-danger' }}">{{ ($item->status_akun == 1) ? 'Telah Disetujui' : 'Belum Disetujui' }}</span></td>
              <td>
                <img src="{{asset('image/'.$item->profil)}}" style="width: 50px" alt="">
              </td>
              <td>{{$item->nik}}</td>
              <td>{{$item->peran}}</td>
              <td>

                  <form action="/manage/anggota/{{$item->id}}" method="POST">
                        @csrf
                        <a href="{{ route('manage-akun.show',$item->id) }}" class="btn btn-info btn-sm"><i class="fas fa-eye"></i></a>
                        <a href="{{ route('manage-akun.edit',$item->id) }}" class="btn btn-warning btn-sm"><i class="fas fa-pen"></i></a>
                  </form>
              </td>
          </tr>
      @empty
         <tr>
             <td>Data Masih Kosong</td>
         </tr>
      @endforelse
  </tbody>
</table>
</div>
@endsection

@section('scripts')
  <script type="text/javascript">
      $(document).ready(function(){
          // btn refresh 
            $('.btn-refresh').click(function(e){
                e.preventDefault();
                $('.preloader').fadeIn();
                location.reload();
            })
       })
  </script>
@endsection 



