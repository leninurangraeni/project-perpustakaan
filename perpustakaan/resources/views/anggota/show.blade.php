@extends('layout.dasbor')
 
@section('judul')
 Detail Anggota {{$userdata->name}}
 @endsection

 @section('konten')
 <h1>{{$userdata->name}}</h1>
 <p>{{$userdata->status_akun}}</p>
 <p>{{$userdata->profil}}</p>
 <p>{{$userdata->nik}}</p>
 <p>{{$userdata->roles_id}}</p>


<a href="{{ route('manage-akun.index') }}" class="btn btn-secondary">Back</a>

 @endsection