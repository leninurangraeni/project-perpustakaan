@extends('layout.dasbor')
 
@section('judul')
Tambah Anggota
@endsection

@section('konten')
<form action="{{ route('manage-akun.store') }}" method="post" enctype="multipart/form-data">
    @csrf 
    <div class="form-group">
      <input type="text" name="name" class="form-control form-control-user" id="exampleFirstName"
          placeholder="Full Name">
    </div>
    @error('name')
       <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <input type="email" name="email" class="form-control form-control-user" id="exampleInputEmail"
          placeholder="Email Address">
    </div>
    @error('email')
       <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group row">
      <div class="col-sm-6 mb-3 mb-sm-0">
        <input type="password" class="form-control form-control-user" name="password"
            id="exampleInputPassword" placeholder="Password ">
       </div>
       @error('password')
          <div class="alert alert-danger">{{ $message }}</div>
       @enderror
       <div class="col-sm-6">
          <input type="password" class="form-control form-control-user" name="password_confirmation"
              id="exampleRepeatPassword" placeholder="Repeat Password">
       </div>
        @error('password_confirmation')
           <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
      <input type="number" name="status_akun"  class="form-control form-control-user" id="exampleroles"
          placeholder="status_akun">
    </div>
    @error('status_akun')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group mb-4">
      <input type="file" name="profil"  class="form-control form-control-user" id="exampleprofil"
          placeholder="profil">
    </div>
    @error('profil')
       <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <input type="number" name="nik"  class="form-control form-control-user" id="exampleNIK"
          placeholder="NIK">
    </div>
    <div class="form-group">
      <select class="form-control" name="roles_id" id="exampleRepeatPassword"  required="">
          <option value=> Role </option>
          <option value="1">1</option>
          <option value="2">2</option>
      </select>
    </div>
    @error('roles_id')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Register Account</button>
    <hr>
</form>

@endsection