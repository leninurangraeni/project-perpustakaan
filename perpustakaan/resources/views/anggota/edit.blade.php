@extends('layout.dasbor')
@section('judul')
Edit Anggota 
@endsection

@section('konten')
  @foreach ($userdata as $item)
  <form action="{{ route('manage-akun.update',$item->id) }}" method="POST">
      @csrf
      @method('put')
      <div class="form-group">
        <label >Nama Anggota</label>
        <input type="text" value="{{ $item->name }}" name="name" class="form-control">
      </div>
      @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label >Status</label>
        <input type="number" value="{{ $item->status_akun }}" name="status_akun" class="form-control">
      </div>
      @error('status_akun')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label >nik</label>
        <input type="number" value="{{ $item->nik }}" name="nik" class="form-control">
      </div>
      @error('nik')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <select class="form-control" value="{{ $item->roles_id }}" name="roles_id" id="exampleRepeatPassword"  required="">
            <option value=> Role </option>
            <option value="1">1</option>
            <option value="2">2</option>
        </select>
    </div>
      <button type="submit" class="btn btn-primary">Update</button>
      @error('roles_id')
       <div class="alert alert-danger">{{ $message }}</div>
      @enderror
  </form>
  @endforeach
@endsection
